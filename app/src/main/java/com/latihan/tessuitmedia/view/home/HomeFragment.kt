package com.latihan.tessuitmedia.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.latihan.tessuitmedia.R
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_next.setOnClickListener {
            if (et_name.text.isNotEmpty()){
                var username = et_name.text.toString()
                val action = HomeFragmentDirections.actionHomeFragmentToSelectEventFragment(username)
                Navigation.findNavController(it).navigate(action)
            }else{
                Toast.makeText(it.context,"masukan nama anda terlebih dahulu",Toast.LENGTH_LONG).show()
            }
        }
    }

}
