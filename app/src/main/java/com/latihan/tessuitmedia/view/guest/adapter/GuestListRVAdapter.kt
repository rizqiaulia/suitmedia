package com.latihan.tessuitmedia.view.guest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.latihan.tessuitmedia.R
import com.latihan.tessuitmedia.listener.OnItemClick
import com.latihan.tessuitmedia.model.Guest
import kotlinx.android.synthetic.main.item_guest.view.*

class GuestListRVAdapter(val listGuest: List<Guest>) :
    RecyclerView.Adapter<GuestListRVAdapter.ViewHolder>() {

    private var itemClick: OnItemClick? = null

    fun onItemCLick(itemClick: OnItemClick) {
        this.itemClick = itemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_guest, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = listGuest.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listGuest[position])

        holder.itemView.setOnClickListener {
            itemClick?.onClick(position, it)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Guest) {
            with(itemView) {

                tv_guest_name.text = item.name

            }
        }

    }
}