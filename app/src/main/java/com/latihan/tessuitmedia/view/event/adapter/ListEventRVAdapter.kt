package com.latihan.tessuitmedia.view.event.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.latihan.tessuitmedia.R
import com.latihan.tessuitmedia.listener.OnItemClick
import com.latihan.tessuitmedia.model.Event
import kotlinx.android.synthetic.main.item_event.view.*

class ListEventRVAdapter(val listEvent: List<Event>) :
    RecyclerView.Adapter<ListEventRVAdapter.ViewHolder>() {

    private var itemClick: OnItemClick? = null

    fun onItemCLick(itemClick: OnItemClick) {
        this.itemClick = itemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_event, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = listEvent.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listEvent[position])

        holder.itemView.setOnClickListener {
            itemClick?.onClick(position,it)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Event) {

            with(itemView) {
                tv_date_event.text = item.tanggal
                tv_event_name.text = item.nama

                Glide.with(itemView).load(item.image).placeholder(R.drawable.ic_launcher_background)
                    .into(iv_event)
            }

        }

    }

}