package com.latihan.tessuitmedia.view.guest

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.latihan.tessuitmedia.api.ApiInterface
import com.latihan.tessuitmedia.model.Guest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GuestViewModel : ViewModel() {

    var apiLoadError = MutableLiveData<String>()
    var guestList = MutableLiveData<List<Guest>>()
    var isLoading = MutableLiveData<Boolean>()

    fun getGuestList() {
        isLoading.value = true
        ApiInterface.create().getGuest().enqueue(object : Callback<List<Guest>> {
            override fun onFailure(call: Call<List<Guest>>, t: Throwable) {
                apiLoadError.value = t.localizedMessage
                isLoading.value = false
            }

            override fun onResponse(call: Call<List<Guest>>, response: Response<List<Guest>>) {

                if (response.isSuccessful) {
                    isLoading.value = false
                    guestList.value = response.body()
                }else{
                    isLoading.value = false
                    apiLoadError.value = response.message()
                }
            }

        })
    }
}