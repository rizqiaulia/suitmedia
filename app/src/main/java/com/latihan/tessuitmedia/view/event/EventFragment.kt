package com.latihan.tessuitmedia.view.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.latihan.tessuitmedia.R
import com.latihan.tessuitmedia.listener.OnItemClick
import com.latihan.tessuitmedia.model.Event
import com.latihan.tessuitmedia.model.Guest
import com.latihan.tessuitmedia.view.event.adapter.ListEventRVAdapter
import kotlinx.android.synthetic.main.fragment_event.*

/**
 * A simple [Fragment] subclass.
 */
class EventFragment : Fragment() {

    var listEvent = ArrayList<Event>()
    lateinit var adapter: ListEventRVAdapter

    var userName: String? = null
    var guestObj: Guest? = null
    var eventName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            userName =EventFragmentArgs.fromBundle(it).name
            guestObj =EventFragmentArgs.fromBundle(it).guest
            eventName =EventFragmentArgs.fromBundle(it).eventName
        }

        adapter = ListEventRVAdapter(listEvent)
        setDummyData()

        rv_list_event.apply {
            layoutManager = LinearLayoutManager(activity)
        }
        rv_list_event.adapter = adapter

        adapter.onItemCLick(object : OnItemClick {
            override fun onClick(position: Int, view: View) {
                val action = EventFragmentDirections.actionEventFragmentToSelectEventFragment(
                    userName,
                    listEvent[position].nama,
                    guestObj
                )
                Navigation.findNavController(view).navigate(action)
            }

        })
    }

    fun setDummyData() {

        val event1 = Event(
            1,
            "https://kumahakonveksi.com/po-content/uploads/maxresdefault.jpg",
            "Anniv Party",
            "2020-12-20"
        )
        val event2 = Event(
            2,
            "https://img.global.news.samsung.com/global/wp-content/uploads/2019/04/A-Galaxy-Event_Bangkok_main_1.jpg",
            "Launching Samsung",
            "2020-04-20"
        )
        val event3 = Event(
            3,
            "https://suitmedia.com/files/works/Thumbnail/sharinghappines.jpg",
            "Event Rumah Zakat",
            "2020-06-22"
        )
        val event4 = Event(
            4,
            "https://suitmedia.com/files/works/Thumbnail/Posh.jpg",
            "Event Posh",
            "2020-07-01"
        )

        listEvent.add(event1)
        listEvent.add(event2)
        listEvent.add(event3)
        listEvent.add(event4)
        adapter.notifyDataSetChanged()
    }

}
