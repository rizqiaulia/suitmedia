package com.latihan.tessuitmedia.view.guest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.latihan.tessuitmedia.R
import com.latihan.tessuitmedia.listener.OnItemClick
import com.latihan.tessuitmedia.model.Guest
import com.latihan.tessuitmedia.view.guest.adapter.GuestListRVAdapter
import kotlinx.android.synthetic.main.fragment_guest.*

/**
 * A simple [Fragment] subclass.
 */
class GuestFragment : Fragment() {

    var userName: String? = null
    var guestObj: Guest? = null
    var eventName: String? = null
    var listGuest = ArrayList<Guest>()

    lateinit var adapter: GuestListRVAdapter

    lateinit var viewModel: GuestViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guest, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            userName = GuestFragmentArgs.fromBundle(it).name
            guestObj = GuestFragmentArgs.fromBundle(it).guest
            eventName = GuestFragmentArgs.fromBundle(it).eventName
        }

        viewModel = ViewModelProviders.of(this).get(GuestViewModel::class.java)
        viewModel.getGuestList()

        adapter = GuestListRVAdapter(listGuest)

        rv_list_guest.apply {
            layoutManager = GridLayoutManager(activity, 2)
        }
        rv_list_guest.adapter = adapter

        adapter.onItemCLick(object :OnItemClick{
            override fun onClick(position: Int, view: View) {
                val action = GuestFragmentDirections.actionGuestFragmentToSelectEventFragment(userName,eventName,listGuest[position])
                Navigation.findNavController(view).navigate(action)
            }

        })

        observeViewModel()
    }

    fun observeViewModel() {
        viewModel.apiLoadError.observe(viewLifecycleOwner, Observer {
            Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
        })

        viewModel.guestList.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                listGuest.clear()
                listGuest.addAll(it)
                adapter.notifyDataSetChanged()
            }
        })

        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it){
                pb_loading.visibility = View.VISIBLE
            }else{
                pb_loading.visibility = View.GONE
            }
        })

    }

}
