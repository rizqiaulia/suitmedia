package com.latihan.tessuitmedia.view.selectEventGuest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.latihan.tessuitmedia.R
import com.latihan.tessuitmedia.model.Guest
import com.latihan.tessuitmedia.util.getSplittedString
import kotlinx.android.synthetic.main.fragment_select_event.*

/**
 * A simple [Fragment] subclass.
 */
class SelectEventFragment : Fragment() {

    var userName: String? = null
    var guestObj: Guest? = null
    var eventName: String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_event, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            userName = SelectEventFragmentArgs.fromBundle(it).name
            eventName = SelectEventFragmentArgs.fromBundle(it).eventName
            guestObj = SelectEventFragmentArgs.fromBundle(it).guest

        }

        if (userName != null) {
            tv_username.text = "Nama : $userName"
        }

        if (eventName != null) {

            btn_event.text = eventName
        }

        if (guestObj != null) {
            btn_guest.text = guestObj!!.name

            val splitDate = getSplittedString(guestObj!!.birthdate, "-")
            val date = Integer.parseInt(splitDate[2])

            if (date %2 == 0 && date % 3 == 0) {
                Toast.makeText(view.context,"IOS", Toast.LENGTH_LONG).show()
            }else if (date %2 == 0){
                Toast.makeText(view.context,"Blackberry", Toast.LENGTH_LONG).show()
            }else if(date % 3 == 0){
                Toast.makeText(view.context,"Android", Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(view.context,"feature phone", Toast.LENGTH_LONG).show()
            }
        }

        btn_event.setOnClickListener {
            val action = SelectEventFragmentDirections.actionSelectEventFragmentToEventFragment(
                userName,
                eventName,
                guestObj
            )
            Navigation.findNavController(it).navigate(action)
        }

        btn_guest.setOnClickListener {
            val action = SelectEventFragmentDirections.actionSelectEventFragmentToGuestFragment(
                userName,
                eventName,
                guestObj
            )
            Navigation.findNavController(it).navigate(action)
        }
    }
}
