package com.latihan.tessuitmedia.api

import com.latihan.tessuitmedia.BuildConfig
import com.latihan.tessuitmedia.model.Guest
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiInterface {

    @GET("v2/596dec7f0f000023032b8017")
    fun getGuest(): Call<List<Guest>>

    companion object{
        fun create(): ApiInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}