package com.latihan.tessuitmedia.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Guest(
    var id: Int,
    var name: String,
    var birthdate: String
) : Parcelable