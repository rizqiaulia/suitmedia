package com.latihan.tessuitmedia.model

data class Event(
    var id:Int,
    var image:String,
    var nama:String,
    var tanggal:String
)