package com.latihan.tessuitmedia.util

fun getSplittedString(text: String, regex: String?): Array<String> {
    return text.split(regex!!).toTypedArray()
}