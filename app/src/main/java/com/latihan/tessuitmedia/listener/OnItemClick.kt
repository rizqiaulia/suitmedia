package com.latihan.tessuitmedia.listener

import android.view.View

interface OnItemClick {
    fun onClick(position: Int,view: View)
}